<?php

/**

||-> Shortcode: Icon List Item

*/
function themeslr_icon_list_item($params, $content) {
  extract( shortcode_atts( 
      array(
          'icon_fontawesome'      => '',
          'list_icon_size'      => '',
          'list_icon_margin'      => '',
          'list_icon_margin_left'      => '',
          'list_icon_color'    => '',
          'list_icon_title'     => '',
          'list_icon_url'     => '',
          'list_icon_title_size'     => '',
          'list_icon_title_color'     => '',
          'alignment'     => '',
      ), $params ) );


  $margin_right = '0';
  $margin_left = '0';

  // margin right
  if ($list_icon_margin) {
    $margin_right = $list_icon_margin;
  }
  // margin left
  if ($list_icon_margin_left) {
    $margin_left = $list_icon_margin_left;
  }


  $html = '';
  $html .= '<div class="tslr-icon-list-item '.$alignment.'">';

              if (!empty($list_icon_url)) {
                $html .= '<a href="'.$list_icon_url.'">';
              }

      $html .= '<div class="tslr-icon-list-icon-holder">
                  <div class="tslr-icon-list-icon-holder-inner clearfix"><i style="margin-left:'.esc_attr($margin_left).'px; margin-right:'.esc_attr($margin_right).'px; color:'.esc_attr($list_icon_color).';font-size:'.esc_attr($list_icon_size).'px" class="'.esc_attr($icon_fontawesome).'"></i></div>
                </div>
                <p class="tslr-icon-list-text" style="font-size: '.esc_attr($list_icon_title_size).'px; color: '.esc_attr($list_icon_title_color).'">'.esc_attr($list_icon_title).'</p>';
              
              if (!empty($list_icon_url)) {
                $html .= '</a>';
              }

            $html .= '</div>';

  return $html;
}
add_shortcode('mt_icon_list_item', 'themeslr_icon_list_item');








/**

||-> Map Shortcode in Visual Composer with: vc_map();

*/
if (function_exists('vc_map')) {
    
  require_once __DIR__ . '/../vc-shortcodes.inc.arrays.php';

  vc_map( array(
     "name" => esc_attr__("ThemeSLR - Icon List Item", 'themeslr'),
     "base" => "mt_icon_list_item",
     "category" => esc_attr__('ThemeSLR', 'themeslr'),
    'show_settings_on_create' => true,
     "icon" => "themeslr_shortcode",
     "params" => array(
      //   array(
      //   'type' => 'dropdown',
      //   'heading' => esc_html__( 'Icon library', 'js_composer' ),
      //   'value' => array(
      //     esc_html__( 'Font Awesome 5', 'js_composer' ) => 'fontawesome',
      //     esc_html__( 'Open Iconic', 'js_composer' ) => 'openiconic',
      //     esc_html__( 'Typicons', 'js_composer' ) => 'typicons',
      //     esc_html__( 'Entypo', 'js_composer' ) => 'entypo',
      //     esc_html__( 'Linecons', 'js_composer' ) => 'linecons',
      //     esc_html__( 'Mono Social', 'js_composer' ) => 'monosocial',
      //     esc_html__( 'Material', 'js_composer' ) => 'material',
      //   ),
      //   'admin_label' => true,
      //   'param_name' => 'type',
      //   'description' => esc_html__( 'Select icon library.', 'js_composer' ),
      // ),
      array(
          "group" => "Icon Setup",
        'type' => 'iconpicker',
        'heading' => esc_html__( 'Icon', 'js_composer' ),
        'param_name' => 'icon_fontawesome',
        'value' => 'fas fa-adjust',
        // default value to backend editor admin_label
        'settings' => array(
          'emptyIcon' => false,
          // default true, display an "EMPTY" icon?
          'iconsPerPage' => 500,
          // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
        ),
        // 'dependency' => array(
        //   'element' => 'type',
        //   'value' => 'fontawesome',
        // ),
        'description' => esc_html__( 'Select icon from library.', 'js_composer' ),
      ),
      // array(
      //   'type' => 'iconpicker',
      //   'heading' => esc_html__( 'Icon', 'js_composer' ),
      //   'param_name' => 'icon_openiconic',
      //   'value' => 'vc-oi vc-oi-dial',
      //   // default value to backend editor admin_label
      //   'settings' => array(
      //     'emptyIcon' => false,
      //     // default true, display an "EMPTY" icon?
      //     'type' => 'openiconic',
      //     'iconsPerPage' => 4000,
      //     // default 100, how many icons per/page to display
      //   ),
      //   'dependency' => array(
      //     'element' => 'type',
      //     'value' => 'openiconic',
      //   ),
      //   'description' => esc_html__( 'Select icon from library.', 'js_composer' ),
      // ),
      // array(
      //   'type' => 'iconpicker',
      //   'heading' => esc_html__( 'Icon', 'js_composer' ),
      //   'param_name' => 'icon_typicons',
      //   'value' => 'typcn typcn-adjust-brightness',
      //   // default value to backend editor admin_label
      //   'settings' => array(
      //     'emptyIcon' => false,
      //     // default true, display an "EMPTY" icon?
      //     'type' => 'typicons',
      //     'iconsPerPage' => 4000,
      //     // default 100, how many icons per/page to display
      //   ),
      //   'dependency' => array(
      //     'element' => 'type',
      //     'value' => 'typicons',
      //   ),
      //   'description' => esc_html__( 'Select icon from library.', 'js_composer' ),
      // ),
      // array(
      //   'type' => 'iconpicker',
      //   'heading' => esc_html__( 'Icon', 'js_composer' ),
      //   'param_name' => 'icon_entypo',
      //   'value' => 'entypo-icon entypo-icon-note',
      //   // default value to backend editor admin_label
      //   'settings' => array(
      //     'emptyIcon' => false,
      //     // default true, display an "EMPTY" icon?
      //     'type' => 'entypo',
      //     'iconsPerPage' => 4000,
      //     // default 100, how many icons per/page to display
      //   ),
      //   'dependency' => array(
      //     'element' => 'type',
      //     'value' => 'entypo',
      //   ),
      // ),
      // array(
      //   'type' => 'iconpicker',
      //   'heading' => esc_html__( 'Icon', 'js_composer' ),
      //   'param_name' => 'icon_linecons',
      //   'value' => 'vc_li vc_li-heart',
      //   // default value to backend editor admin_label
      //   'settings' => array(
      //     'emptyIcon' => false,
      //     // default true, display an "EMPTY" icon?
      //     'type' => 'linecons',
      //     'iconsPerPage' => 4000,
      //     // default 100, how many icons per/page to display
      //   ),
      //   'dependency' => array(
      //     'element' => 'type',
      //     'value' => 'linecons',
      //   ),
      //   'description' => esc_html__( 'Select icon from library.', 'js_composer' ),
      // ),
      // array(
      //   'type' => 'iconpicker',
      //   'heading' => esc_html__( 'Icon', 'js_composer' ),
      //   'param_name' => 'icon_monosocial',
      //   'value' => 'vc-mono vc-mono-fivehundredpx',
      //   // default value to backend editor admin_label
      //   'settings' => array(
      //     'emptyIcon' => false,
      //     // default true, display an "EMPTY" icon?
      //     'type' => 'monosocial',
      //     'iconsPerPage' => 4000,
      //     // default 100, how many icons per/page to display
      //   ),
      //   'dependency' => array(
      //     'element' => 'type',
      //     'value' => 'monosocial',
      //   ),
      //   'description' => esc_html__( 'Select icon from library.', 'js_composer' ),
      // ),
      // array(
      //   'type' => 'iconpicker',
      //   'heading' => esc_html__( 'Icon', 'js_composer' ),
      //   'param_name' => 'icon_material',
      //   'value' => 'vc-material vc-material-cake',
      //   // default value to backend editor admin_label
      //   'settings' => array(
      //     'emptyIcon' => false,
      //     // default true, display an "EMPTY" icon?
      //     'type' => 'material',
      //     'iconsPerPage' => 4000,
      //     // default 100, how many icons per/page to display
      //   ),
      //   'dependency' => array(
      //     'element' => 'type',
      //     'value' => 'material',
      //   ),
      //   'description' => esc_html__( 'Select icon from library.', 'js_composer' ),
      // ),
        // array(
        //   "group" => "Icon Setup",
        //   "type" => "dropdown",
        //   "heading" => esc_attr__("Icon", 'themeslr'),
        //   "param_name" => "list_icon",
        //   "std" => '',
        //   "holder" => "div",
        //   "class" => "",
        //   "description" => "",
        //   "value" => $fa_list
        // ),
        array(
          "group" => "Options",
          "type" => "dropdown",
          "heading" => esc_attr__("Alignment", 'themeslr'),
          "param_name" => "alignment",
          "value" => array(
            esc_attr__('Left', 'themeslr')   => 'text-left',
            esc_attr__('Center', 'themeslr')   => 'text-center',
            esc_attr__('Right', 'themeslr')   => 'text-right'
            ),
          "std" => 'normal',
          "holder" => "div",
          "class" => "",
          "description" => ""
        ),
        array(
          "group" => "Icon Setup",
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__("Icon Size (px)", 'themeslr'),
          "param_name" => "list_icon_size",
          "value" => "",
          "description" => "Default: 18(px)"
        ),
        array(
          "group" => "Icon Setup",
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__("Icon Margin Right (px)", 'themeslr'),
          "param_name" => "list_icon_margin",
          "value" => "",
          "description" => ""
        ),
        array(
          "group" => "Icon Setup",
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__("Icon Margin Left (px)", 'themeslr'),
          "param_name" => "list_icon_margin_left",
          "value" => "",
        ),
        array(
          "group" => "Icon Setup",
          "type" => "colorpicker",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__("Icon Color", 'themeslr'),
          "param_name" => "list_icon_color",
          "value" => "",
        ),
        array(
          "group" => "Label Setup",
          "type" => "textfield",
          "heading" => esc_attr__("Label/Title", 'themeslr'),
          "param_name" => "list_icon_title",
          "std" => '',
          "holder" => "div",
          "class" => "",
          "description" => "Eg: This is a label"
        ),
        array(
          "group" => "Label Setup",
          "type" => "textfield",
          "heading" => esc_attr__("Label/Icon URL", 'themeslr'),
          "param_name" => "list_icon_url",
          "std" => '',
          "holder" => "div",
          "class" => "",
          "description" => "Eg: http://themeslr.com"
        ),
        array(
          "group" => "Label Setup",
          "type" => "textfield",
          "heading" => esc_attr__("Title Font Size", 'themeslr'),
          "param_name" => "list_icon_title_size",
          "std" => '',
          "holder" => "div",
          "class" => "",
          "description" => "Default: 18(px)"
        ),
        array(
          "group" => "Label Setup",
          "type" => "colorpicker",
          "heading" => esc_attr__("Title Color", 'themeslr'),
          "param_name" => "list_icon_title_color",
          "std" => '',
          "holder" => "div",
          "class" => "",
          "description" => ""
        ),
     )
  ));
}