<?php

function themeslr_isolated_element_shortcode( $params, $content ) {
    extract( shortcode_atts( 
        array(
            'elements'                       => '',
        ), $params ) );
    
    $shortcode_content = '';
    if (function_exists('vc_param_group_parse_atts')) {
        $elements = vc_param_group_parse_atts($params['elements']);

        if ($elements) {
            foreach($elements as $element){
                $image_attributes = wp_get_attachment_image_src( $element['image'], 'full' );
                if ($image_attributes) {
                    $shortcode_content .= '<img class="absolute" width="'.$element['width'].'" height="auto" style="left: '.$element['left_percent'].'%; top: '.$element['top_percent'].'%;" src="'.$image_attributes[0].'" alt="absolute element" />';
                }
            }
        }
    }
    

    return $shortcode_content;
}
add_shortcode('tslr-isolated-element', 'themeslr_isolated_element_shortcode');



if ( function_exists('vc_map') ) {
    vc_map( 
      array(
       "name" => esc_attr__("ThemeSLR - Isolated Element", 'themeslr'),
       "base" => "tslr-isolated-element",
       "icon" => "themeslr_shortcode",
       "category" => esc_attr__('ThemeSLR', 'themeslr'),
       "params" => array(
            array(
                "group" => esc_attr__( "Settings", 'themeslr' ),
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'elements',
                // Note params is mapped inside param-group:
                'params' => array(
                    array(
                        "type" => "attach_image",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_attr__("Element Image", 'themeslr'),
                        "param_name" => "image",
                    ),
                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_attr__("Left (%) - Do not write the '%'", 'themeslr'),
                        "param_name" => "left_percent",
                    ),
                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_attr__("Top (%) - Do not write the '%'", 'themeslr'),
                        "param_name" => "top_percent",
                    ),
                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_attr__("Width (px) - Do not write the 'px'", 'themeslr'),
                        "param_name" => "width",
                    ),
                )
            ),
       )
    ));  
}
