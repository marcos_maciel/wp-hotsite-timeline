<?php

function themeslr_row_overlay( $params, $content ) {
    extract( shortcode_atts( 
        array(
            'background'                       => '',
            'inner_column'                       => '',
        ), $params ) );
    
    if ($inner_column) {
        $inner_column = 'yes';
    }else{
        $inner_column = ' ';
    }

    $shortcode_content = '';
    $shortcode_content .= '<div data-inner-column="'.esc_attr($inner_column).'" style="background: '.esc_html($background).'" class="themeslr-row-overlay"></div>';

    return $shortcode_content;
}
add_shortcode('tslr-row-overlay', 'themeslr_row_overlay');



if ( function_exists('vc_map') ) {
    vc_map( 
        array(
            "name" => esc_attr__("ThemeSLR - Row Overlay", 'themeslr'),
            "base" => "tslr-row-overlay",
            "icon" => "themeslr_shortcode",
            "category" => esc_attr__('ThemeSLR', 'themeslr'),
            "params" => array(
                array(
                    "type" => "colorpicker",
                    "holder" => "div",
                    "class" => "",
                    "heading" => esc_attr__("Background Color", 'themeslr'),
                    "param_name" => "background",
                ),
                array(
                    "type" => "checkbox",
                    "class" => "",
                    "heading" => __( "Keep in Column?", "themeslr" ),
                    "param_name" => "inner_column",
                    "description" => __( "If checked, the overlay will be only applied in a column. By default, it will be applied on row.", "themeslr" )
                ),
           )
        )
    );  
}
