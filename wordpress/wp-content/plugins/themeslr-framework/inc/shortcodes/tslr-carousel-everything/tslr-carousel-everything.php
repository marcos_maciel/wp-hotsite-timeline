<?php
/**
||-> Shortcode: Knowledge List Accordion
*/
function themeslr_carousel_everything_holder($params,  $content = NULL) {
    extract( shortcode_atts( 
        array(
            'slide_bg_image'       =>'',
            'heading_title'       =>'',
        ), $params ) );

  $html = '';

  $id = 'carousel_everything_'.uniqid();

  $bg_image_style = '';
  if ($slide_bg_image) {
    $bg_image_url = wp_get_attachment_url( $slide_bg_image );
    $bg_image_style = 'background-image:url('.$bg_image_url.');background-position: center;background-repeat: no-repeat;background-size: auto 100%;';
  }

  $html .= '<div style="'.$bg_image_style.'" class="tslr-carousel-everything-shortcode-group">';
    $html .= '<div class="tslr-carousel-everything-shortcode owl-carousel owl-theme" id="'.$id.'">';
      $html .= do_shortcode($content);
    $html .= '</div>';
  $html .= '</div>';

  return $html;
}
add_shortcode('themeslr_carousel_everything_holder_shortcode', 'themeslr_carousel_everything_holder');
/**
||-> Shortcode: Child Shortcode v1
*/
function themeslr_carousel_everything_inner_content($params, $content = NULL) {
  extract( shortcode_atts( 
    array(
        'slide_heading' => '',
        'slide_heading_color' => '',
        'slide_subheading' => '',
        'slide_subheading_color' => '',
        'slide_paragraph' => '',
        'slide_paragraph_color' => '',
        'slide_button' => '',
        'slide_button_link' => '',
    ), $params )
  );

  $html = '';

  $html .= '<div class="relative tslr-carousel-everything-slide">';
    $html .= '<h2 class="tslr-carousel-everything-slide--heading" style="color:'.$slide_heading_color.';">'.$slide_heading.'</h2>';
    $html .= '<h5 class="tslr-carousel-everything-slide--subheading" style="color:'.$slide_subheading_color.';">'.$slide_subheading.'</h5>';
    $html .= '<p class="tslr-carousel-everything-slide--paragraph" style="color:'.$slide_paragraph_color.';">'.$slide_paragraph.'</p>';
    $html .= '<div class="tslr-carousel-everything-slide--button">'.do_shortcode('[mt-bootstrap-button btn_text="'.$slide_button.'" btn_url="'.$slide_button_link.'" btn_size="btn btn-lg" align="text-left" color="#f26226" border_color="#2e62af" text_color="#fffffa"]').'</div>';
  $html.='</div>';

  return $html;
}
add_shortcode('themeslr_carousel_everything_inner_content_shortcode', 'themeslr_carousel_everything_inner_content');
/**
||-> Map Shortcode in Visual Composer with: vc_map();
*/
if ( function_exists('vc_map') ) {
    vc_map( array(
        "name" => esc_attr__("ThemeSLR - Carousel Everything", 'themeslr'),
        "base" => "themeslr_carousel_everything_holder_shortcode",
        "as_parent" => array('only' => 'themeslr_carousel_everything_inner_content_shortcode,vc_column_text'), 
        "content_element" => true,
        "show_settings_on_create" => true,
        "category" => esc_attr__('ThemeSLR', 'themeslr'),
        "is_container" => true,
        "params" => array(
            // add params same as with any other content element 
           array(
              "group" => "Options",
              "type" => "textfield",
              "holder" => "div",
              "class" => "",
              "heading" => esc_attr__( "Heading title", 'themeslr' ),
              "param_name" => "heading_title",
              "value" => "",
              "description" => ""
            ),
            array(
              "group" => "Styling",
              "type" => "attach_image",
              "class" => "",
              "heading" => esc_attr__( "Background Image", 'themeslr' ),
              "param_name" => "slide_bg_image",
              "value" => "", //Default color
              "description" => esc_attr__( "Default: Semitransparent logo", 'themeslr' )
            )
        ),
        "js_view" => 'VcColumnView'
    ) );
    vc_map( array(
        "name" => esc_attr__("ThemeSLR - Carousel Slide", 'themeslr'),
        "base" => "themeslr_carousel_everything_inner_content_shortcode",
        "content_element" => true,
        "as_child" => array('only' => 'themeslr_carousel_everything_holder_shortcode'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
          array(
            "group" => "Heading",
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__( "Slide Heading", 'themeslr' ),
            "param_name" => "slide_heading",
            "value" => "",
            "description" => esc_attr__( "Type a heading for the current slide", 'themeslr' )
          ),
          array(
            "group" => "Heading",
            "type" => "colorpicker",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__( "Slide Heading Color", 'themeslr' ),
            "param_name" => "slide_heading_color",
            "value" => "",
          ),
          array(
            "group" => "Subheading",
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__( "Slide Subheading", 'themeslr' ),
            "param_name" => "slide_subheading",
            "value" => "",
            "description" => esc_attr__( "Type a sub heading for the current slide", 'themeslr' )
          ),
          array(
            "group" => "Subheading",
            "type" => "colorpicker",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__( "Slide Subheading Color", 'themeslr' ),
            "param_name" => "slide_subheading_color",
            "value" => "",
          ),
          array(
            "group" => "Paragraph",
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__( "Slide Paragraph", 'themeslr' ),
            "param_name" => "slide_paragraph",
            "value" => "",
            "description" => esc_attr__( "Type a paragraph for the current slide", 'themeslr' )
          ),
          array(
            "group" => "Paragraph",
            "type" => "colorpicker",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__( "Slide Paragraph Color", 'themeslr' ),
            "param_name" => "slide_paragraph_color",
            "value" => "",
          ),
          array(
            "group" => "Button",
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__( "Slide Button Text", 'themeslr' ),
            "param_name" => "slide_button",
            "value" => "",
          ),
          array(
            "group" => "Button",
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__( "Slide Button Link", 'themeslr' ),
            "param_name" => "slide_button_link",
            "value" => "",
          ),
        )
    ) );
    //Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
    if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
        class WPBakeryShortCode_themeslr_carousel_everything_holder_shortcode extends WPBakeryShortCodesContainer {
        }
    }
    if ( class_exists( 'WPBakeryShortCode' ) ) {
        class WPBakeryShortCode_themeslr_carousel_everything_inner_content extends WPBakeryShortCode {
        }
    }
}