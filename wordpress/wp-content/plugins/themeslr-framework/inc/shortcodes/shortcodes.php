<?php 
// SHORTCODES
include_once( 'tslr-testimonials/tslr-testimonials01.php' ); # Testimonials 01
include_once( 'tslr-clients/tslr-clients.php' ); # Clients
include_once( 'tslr-skills/tslr-skills.php' ); # Skills
include_once( 'tslr-title-subtitle/tslr-title-subtitle.php' ); # Title Subtitle
include_once( 'tslr-pricing-tables/tslr-pricing-tables.php' ); # Pricing Tables
include_once( 'tslr-blog-posts/tslr-blogpost01.php' ); # blogpost01
include_once( 'tslr-icon-list-item/tslr-icon-list-item.php' ); # Mailchimp Subscribe Form
include_once( 'tslr-bootstrap-button/tslr-bootstrap-button.php' ); # Bootstrap Buttons
include_once( 'tslr-bootstrap-listgroup/tslr-bootstrap-listgroup.php' ); # Bootstrap List Group
include_once( 'tslr-tweets/tslr-tweets.php' ); # Bootstrap Buttons
include_once( 'tslr-smooth-gallery/tslr-smooth-gallery.php' ); # Bootstrap Buttons
include_once( 'tslr-carousel-everything/tslr-carousel-everything.php' ); # Bootstrap Buttons
include_once( 'tslr-isolated-element/tslr-isolated-element.php' ); # Bootstrap Buttons
include_once( 'tslr-row-overlay/tslr-row-overlay.php' ); # Bootstrap Buttons
include_once( 'tslr-hero-slider/tslr-hero-slider.php' ); # Bootstrap Buttons


function themeslr_video_popup_shortcode($params, $content) {

    extract( shortcode_atts( 
        array(
            'source_vimeo'              => '',
            'source_youtube'            => '',
            'video_source'              => '',
            'video_label_next_to_icon'              => '',
            'icon_fontawesome'              => '',
            'vimeo_link_id'             => '',
            'youtube_link_id'           => '',
            'button_image_alignment'           => '',
            'button_image'              => '',
            'custom_image_checkbox'              => ''
        ), $params ) );

    $thumb      = wp_get_attachment_image_src($button_image, "full");
	$video_image_class = '';
	if ($thumb && $custom_image_checkbox) {
		$video_image_class = $button_image_alignment;
	}

    $html = '';
	$html .= '<div class="themeslr-video-popup-shortcode '.$video_image_class.'">';
		if ($video_source == 'source_vimeo') {
			$href = "https://vimeo.com/".$vimeo_link_id;
		} elseif ($video_source == 'source_youtube') {
			$href = "https://www.youtube.com/watch?v=".$youtube_link_id;
		}

		$html .= '<a class="themeslr-vimeo-popup" href="'.$href.'">';
			if ($thumb) {
				$html .= '<img class="buton_image_class" src="'.esc_attr($thumb[0]).'" data-src="'.esc_attr($thumb[0]).'" alt="'.esc_attr__('Video Popup', 'themeslr').'">';
			}else{
				$html .= '<i class="'.esc_attr($icon_fontawesome).'"></i> '.esc_html($video_label_next_to_icon);
			}
		$html .= '</a>';
	$html .= '</div>';

    return $html;
}

add_shortcode('themeslr-video-popup-shortcode', 'themeslr_video_popup_shortcode');



if (function_exists('vc_map')) {
	vc_map( 
		array(
			"name" => esc_attr__("ThemeSLR - Video Popup", "themeslr"),
			"base" => "themeslr-video-popup-shortcode",
			"category" => esc_attr__('ThemeSLR', "themeslr"),
			"params" => array(
				array(
					"group" => "Options",
					'type' => 'iconpicker',
					'heading' => esc_html__( 'Icon', 'js_composer' ),
					'param_name' => 'icon_fontawesome',
					'value' => 'fas fa-adjust',
					'settings' => array(
						'emptyIcon' => false,
						'iconsPerPage' => 500,
					),
					'description' => esc_html__( 'Select icon from library.', 'js_composer' ),
				),
				array(
					"group" => esc_attr__("Options", "themeslr"),
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => esc_attr__("Text Label Next to Icon", 'themeslr'),
					"param_name" => "video_label_next_to_icon",
					"value" => "",
					"description" => "Example: How it Works"
				),
                array(
                    "group" => "Options",
                    "type" => "checkbox",
                    "class" => "",
                    "heading" => __( "Custom Image Icon", "themeslr" ),
                    "param_name" => "custom_image_checkbox",
                    "description" => __( "If checked, an image upload field will appear. The text/font icon will be overwritten.", "themeslr" )
                ),
				array(
					"group" => esc_attr__("Options", "themeslr"),
					"type" => "attach_images",
					"holder" => "div",
					"class" => "",
					"heading" => esc_attr__( "Choose image", "themeslr" ),
					"param_name" => "button_image",
					"value" => "",
					"description" => esc_attr__( "Choose image for play button", "themeslr" ),
					'dependency' => array(
					 	'element' => 'custom_image_checkbox',
					  	'value' => 'true',
					),
				),
                array(
                    "group" => "Options",
                    "type" => "dropdown",
                    "holder" => "div",
                    "std" => '',
                    "class" => "",
                    "heading" => esc_attr__("Image Icon Alignment", 'themeslr'),
                    "param_name" => "button_image_alignment",
                    "description" => "",
                    "value" => array(
                        esc_attr__('Left', 'themeslr')     => 'image-left text-left',
                        esc_attr__('Center', 'themeslr')     => 'image-center text-center',
                        esc_attr__('Right', 'themeslr')     => 'image-right text-right'
                    ),
					'dependency' => array(
					 	'element' => 'custom_image_checkbox',
					  	'value' => 'true',
					),
                ),
				array(
					"group" => esc_attr__("Options", "themeslr"),
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => esc_attr__("Video source", "themeslr"),
					"param_name" => "video_source",
					"std" => '',
					"value" => array(
						'Youtube'   => 'source_youtube',
						'Vimeo'     => 'source_vimeo',
					)
				),
				array(
					"group" => esc_attr__("Options", "themeslr"),
					"dependency" => array(
						'element' => 'video_source',
						'value' => array( 'source_vimeo' ),
					),
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => esc_attr__("Vimeo id link", "themeslr"),
					"param_name" => "vimeo_link_id",
				),
				array(
					"group" => esc_attr__("Options", "themeslr"),
					"dependency" => array(
						'element' => 'video_source',
						'value' => array( 'source_youtube' ),
					),
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => esc_attr__("Youtube id link", "themeslr"),
					"param_name" => "youtube_link_id",
				)
			)
		)
	);
}