<?php
/**
* Plugin Name: WC Product Variations Configurator on Any Page
* Plugin URI: https://themeslr.com/
* Description: Configure Variable Products from Variations on Any Page (Shortcode or in text block widgets);
* Version: 1.0.9
* Author: ThemeSLR
* Author https://themeslr.com/
* Text Domain: wcpvc
*/


if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Exit if accessed directly.
require_once ABSPATH . 'wp-admin/includes/plugin.php';

/**
 * Function wcpvc_init_lang: For translations
 */
function wcpvc_init_lang(){
    load_plugin_textdomain('wcpvc', false, dirname( plugin_basename( __FILE__ ) ). '/languages/');
}
add_action('plugins_loaded', 'wcpvc_init_lang');


if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

    /**
     * Function wcpvc_styles_scripts: Adds styles and scripts
     */
    function wcpvc_styles_scripts() {
        // CSS
        wp_register_style( 'wcpvc-custom',  plugin_dir_url( __FILE__ ) . 'assets/css/wcpvc-custom.css' );
        wp_enqueue_style( 'wcpvc-custom' );
        
        // SCRIPTS
        if (class_exists('WooCommerce') && !is_product() && !is_shop() && !is_product_category()) {
            wp_deregister_script( 'wc-add-to-cart-variation' );
            wp_enqueue_script( "wc-add-to-cart-variation", plugin_dir_url( __FILE__ ) . 'assets/js/add-to-cart-variation.js', array( 'jquery', 'wp-util'), "1.0", true );
        }
    }
    add_action( 'wp_enqueue_scripts', 'wcpvc_styles_scripts' );

    // SHORTCODESs
    require_once('inc/shortcodes/shortcodes.php'); 
    // METABOXES
    require_once('inc/metaboxes/metaboxes.php'); 

    if ( ! function_exists( 'wcpvc_products_attributes_radio' ) ) {
        /**
         * Function wcpvc_styles_scripts: Generates Radios buttons
         * @param $product_id: integer      Product ID
         * @param $checked_value: string
         * @param $value: string
         * @param $label: string
         * @param $name: string
         * @param $active_background_color: string      Color Hexa format
         * @param $active_border_color: string      Color Hexa format
         */
        function wcpvc_products_attributes_radio( $product_id, $checked_value, $value, $label, $name, $active_background_color, $active_border_color ) {
            global $product;

            $input_name = 'attribute_' . esc_attr( $name ) ;
            $esc_value = esc_attr( $value );
            $id = esc_attr( $name . '_v_' . $value . $product_id ); //added product ID at the end of the name to target single products
            $checked = checked( $checked_value, $value, false );
            $filtered_label = apply_filters( 'woocommerce_variation_option_name', $label, esc_attr( $name ) );

            $term = get_term_by('slug', $value, $name); 
            $description = $term->description;

            $image_id = get_term_meta( $term->term_id, 'category-image-id', true );
            $image_url = '';
            if ( $image_id ) {
                $image_url = wp_get_attachment_image( $image_id, 'full' );
            }

            printf( '<div class="thecrate-single-variation-box" id="attr-%3$s" data-hover-bg="%8$s" data-hover-border="%9$s">
                        <input class="hidden" type="radio" name="%1$s" value="%2$s" id="%3$s" %4$s>
                        <label for="%3$s">%7$s<div class="clearfix"></div><strong>%5$s</strong>
                        <span class="thecrate-variation-description">%6$s</span></label>
                    </div>', $input_name, $esc_value, $id, $checked, $filtered_label, $description, $image_url, $active_background_color, $active_border_color  );
        }
    }

    if ( ! function_exists( 'wcpvc_get_variable_products' ) ) {
        /**
         * Function wcpvc_get_variable_products: Getting the variable products 
         */
        function wcpvc_get_variable_products() {
            $args = array(
                'post_type' => 'product',
                'posts_per_page' => -1,
                'orderby' => 'name',
                'order'   => 'ASC',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_type',
                        'field'    => 'slug',
                        'terms'    => array('variable', 'variable-subscription'),
                    ),
                ),
             );
            $products = new WP_Query($args);

            $variable_products = array();

            if ( $products->have_posts() ) :
                while ( $products->have_posts() ) : $products->the_post();
                    if ( class_exists('Elementor\Core\Admin\Admin') ) {
                        $variable_products[get_the_ID()] = get_the_title();
                    }else{
                        $variable_products[get_the_title()] = get_the_ID();
                    }
                endwhile;
            endif;
            wp_reset_postdata();

            return $variable_products;
        }
    }
}else{
    add_action( 'admin_notices', 'wcpvc_error_notice' );
    function wcpvc_error_notice() {

        global $current_screen;

        if ( $current_screen->parent_base == 'plugins' ) {
            echo '<div class="error"><p><strong>'.esc_html__('WC Product Variations Configurator on Any Page', 'wcpvc').'</strong> ' . wp_kses_post( __( 'requires <a href="http://www.woothemes.com/woocommerce/" target="_blank">WooCommerce</a> to be activated in order to work. Please install and activate <a href="' . admin_url( 'plugin-install.php?tab=search&type=term&s=WooCommerce' ) . '" target="_blank">WooCommerce</a> first.', 'wcpvc' ) ) . '</p></div>';
        }

    }
}
