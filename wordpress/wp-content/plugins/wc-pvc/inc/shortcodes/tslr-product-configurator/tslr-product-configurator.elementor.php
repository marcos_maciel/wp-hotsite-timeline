<?php
namespace Elementor;

class wcpvc_product_variations_configurator_widget extends Widget_Base {
	
	public function get_name() {
		return 'themeslr_pv_configurator';
	}
	
	public function get_title() {
		return 'ThemeSLR - Product Variations Configurator';
	}
	
	public function get_icon() {
		return 'eicon-gallery-grid';
	}
	
	public function get_categories() {
		return [ 'themeslr-widgets' ];
	}
	
	protected function register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Content', 'wcpvc' ),
			]
		);

		$this->add_control(
			'product_id',
			[
				'label' => __( 'Select a Variable Product', 'wcpvc' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'multiple' => false,
				'label_block' => true,
				'options' => wcpvc_get_variable_products(),
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Section Title', 'wcpvc' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Configure Your Next Crate', 'wcpvc' ),
			]
		);
		
		$this->add_control(
			'subtitle',
			[
				'label' => __( 'Section Subtitle', 'wcpvc' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'The Boxes below are variations of a variable product. With this plugin you can list the variations of a variable product in this nice looking format', 'wcpvc' ),
			]
		);

		$this->add_control(
			'hide_empty_fields',
			[
				'label' => __( 'Hide empty title/subtitle replacement texts? (If the title and the subtitle fields are empty, we have some placeholder texts on the templates. By checking this option, those replacement texts will not be listed.)', 'modeltheme' ),
				'label_block' => true,
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'true' => __( 'Hide', 'modeltheme' ),
					'false' => __( 'Show', 'modeltheme' ),
				]
			]
		);

		$this->add_control(
			'title_subtitle_alignment',
			[
				'label' => __( 'Title/Subtitle Alignment', 'wcpvc' ),
				'label_block' => true,
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'text-left' => __( 'Left', 'wcpvc' ),
					'text-center' => __( 'Center', 'wcpvc' ),
					'text-right' => __( 'Right', 'wcpvc' ),
				]
			]
		);

		$this->add_control(
			'attribute_titles_alignment',
			[
				'label' => __( 'Attribute Titles Alignment', 'wcpvc' ),
				'label_block' => true,
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'text-left' => __( 'Left', 'wcpvc' ),
					'text-center' => __( 'Center', 'wcpvc' ),
					'text-right' => __( 'Right', 'wcpvc' ),
				]
			]
		);

		$this->add_control(
			'attribute_boxes_alignment',
			[
				'label' => __( 'Attribute Boxes Content Alignment', 'wcpvc' ),
				'label_block' => true,
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'text-left' => __( 'Left', 'wcpvc' ),
					'text-center' => __( 'Center', 'wcpvc' ),
					'text-right' => __( 'Right', 'wcpvc' ),
				]
			]
		);

		$this->add_control(
			'active_background_color',
			[
				'label' => __( 'Active Box Background color', 'wcpvc' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				]
			]
		);

		$this->add_control(
			'active_border_color',
			[
				'label' => __( 'Active Box Border color', 'wcpvc' ),
				'label_block' => true,
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Core\Schemes\Color::get_type(),
					'value' => \Elementor\Core\Schemes\Color::COLOR_1,
				]
			]
		);
		$this->add_control(
			'tabs_status',
			[
				'label' => __( 'Attributes in Tabs?', 'modeltheme' ),
				'label_block' => true,
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'true' => __( 'Yes', 'modeltheme' ),
					'false' => __( 'No', 'modeltheme' ),
				]
			]
		);
		$this->add_control(
			'carousel_status',
			[
				'label' => __( 'Enable Carousel (Only on mobiles on resolutions < 767px)', 'modeltheme' ),
				'label_block' => true,
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'true' => __( 'Yes', 'modeltheme' ),
					'false' => __( 'No', 'modeltheme' ),
				]
			]
		);
		$this->add_control(
			'info_before_purchase_btn',
			[
				'label' => __( 'Info Text Before Add to Cart', 'wcpvc' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'The price will be visible here after completing the configuration.', 'wcpvc' ),
			]
		);

		$this->add_control(
			'info_before_purchase_btn_success',
			[
				'label' => __( 'Info Text Before Add to Cart (Success)', 'wcpvc' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Default: Please proceed to Add To Cart!', 'wcpvc' ),
			]
		);

		$this->end_controls_section();

	}

	protected function render() {

        $settings 						= $this->get_settings_for_display();
        $product_id 					= $settings['product_id'];
        $title 							= $settings['title'];
        $subtitle 						= $settings['subtitle'];
        $title_subtitle_alignment		= $settings['title_subtitle_alignment'];
        $attribute_titles_alignment		= $settings['attribute_titles_alignment'];
        $attribute_boxes_alignment 		= $settings['attribute_boxes_alignment'];
        $active_background_color 		= $settings['active_background_color'];
        $active_border_color 			= $settings['active_border_color'];
        $info_before_purchase_btn 			= $settings['info_before_purchase_btn'];
        $info_before_purchase_btn_success 			= $settings['info_before_purchase_btn_success'];
        $tabs_status 			= $settings['tabs_status'];
        $hide_empty_fields 			= $settings['hide_empty_fields'];
        $carousel_status 			= $settings['carousel_status'];

		echo do_shortcode('[themeslr_pv_configurator product_id="'.esc_attr($product_id).'" title="'.esc_attr($title).'" subtitle="'.esc_attr($subtitle).'" title_subtitle_alignment="'.esc_attr($title_subtitle_alignment).'" attribute_titles_alignment="'.esc_attr($attribute_titles_alignment).'" attribute_boxes_alignment="'.esc_attr($attribute_boxes_alignment).'" active_background_color="'.esc_attr($active_background_color).'" active_border_color="'.esc_attr($active_border_color).'" tabs_status="'.esc_attr($tabs_status).'" hide_empty_fields="'.esc_attr($hide_empty_fields).'" carousel_status="'.esc_attr($carousel_status).'" info_before_purchase_btn="'.esc_attr($info_before_purchase_btn).'" info_before_purchase_btn_success="'.esc_attr($info_before_purchase_btn_success).'"]');
	}
	
	
}