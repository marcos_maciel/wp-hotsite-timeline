<?php

/**
||-> Shortcode: Product Variations Configurator - PVC
*/
function wcpvc_product_variations_configurator($params, $content) {
    extract( shortcode_atts( 
        array(
            'product_id'               => '',
            'title'               => '',
            'subtitle'               => '',
            'info_before_purchase_btn'               => '',
            'info_before_purchase_btn_success'               => '',
            'title_subtitle_alignment'               => '',
            'attribute_titles_alignment'               => '',
            'attribute_boxes_alignment'               => '',
            'active_background_color'               => '',
            'active_border_color'               => '',
            'carousel_status'               => '',
            'tabs_status'               => '',
            'hide_empty_fields'               => '',
        ), $params ) ); 

    $title_v = esc_html__('Generate Your Coffee Crate', 'wcpvc');
    $subtitle_v = esc_html__('Buy your personalized coffee crate by choosing the options you want.', 'wcpvc');;
    
    if ($title) {
        $title_v = $title;
    }
    if ($subtitle) {
        $subtitle_v = $subtitle;
    }

    ob_start();


    $carousel_class = '';
    $carousel_item_class = '';
    if ($carousel_status) {
        $carousel_class = 'thecrate-custom-variations-inner-carousel owl-carousel owl-theme';
        $carousel_item_class = 'item';
    
        // OWL Carousel
        wp_enqueue_style( 'owl-carousel2', plugins_url( '../../../assets/css/owl-carousel/owl.carousel.min.css' , __FILE__ ));
        wp_enqueue_script( 'owl-carousel2', plugins_url( '../../../assets/js/owl-carousel/owl.carousel.min.js' , __FILE__));
        wp_enqueue_script( 'modernizr', plugins_url( '../../../assets/js/modernizr/modernizr.custom.js' , __FILE__));
    }

    $tabs_wrapper = '';
    $wcpvc_class = '';
    if ($tabs_status) {
        $tabs_wrapper = 'wcpvc-content-wrap';
        $wcpvc_class = 'is-tabs-enabled';
        wp_enqueue_script( 'cbpFWTabs', plugins_url( '../../../assets/js/cbpFWTabs.js' , __FILE__));
    }
    wp_enqueue_script( 'wcpvc-custom', plugins_url( '../../../assets/js/wcpvc-custom.js' , __FILE__));
    ?>

            
    <?php if($product_id){ ?>
        <div class="thecrate-product-configurator <?php echo esc_attr($wcpvc_class); ?>">
            <?php 
                $product = wc_get_product($product_id);
            
                if($product){
                    if ( $product->has_attributes() ) {
                        $attributes = $product->get_attributes();
                    }
                    $available_variations = $product->get_available_variations();
                    $attribute_keys  = array_keys( $product->get_variation_attributes() );
                    $variations_json = wp_json_encode( $available_variations );
                    $variations_attr = function_exists( 'wc_esc_json' ) ? wc_esc_json( $variations_json ) : _wp_specialchars( $variations_json, ENT_QUOTES, 'UTF-8', true ); ?>
                
                    <?php if($hide_empty_fields == 'false'){ ?>
                        <h2 class="<?php echo esc_attr($title_subtitle_alignment); ?>"><?php echo $title_v; ?></h2>
                        <p class="<?php echo esc_attr($title_subtitle_alignment); ?>"><?php echo $subtitle_v; ?></p>
                    <?php } ?>

                    <form class="variations_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product_id ); ?>" data-product_variations="<?php echo $variations_attr; // WPCS: XSS ok. ?>">
                        <?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
                            <p class="stock out-of-stock"><?php echo esc_html( apply_filters( 'woocommerce_out_of_stock_message', __( 'This product is currently out of stock and unavailable.', 'wcpvc' ) ) ); ?></p>
                        <?php else : ?>

                            <style type="text/css">
                                .thecrate-product-configurator .thecrate-single-variation-box:hover, .thecrate-product-configurator .thecrate-single-variation-box.selected{
                                    border-color: <?php echo esc_attr($active_border_color); ?>;
                                    background: <?php echo esc_attr($active_background_color); ?>;
                                }
                            </style>

                            <div class="variations thecrate-custom-variations-builder" cellspacing="0">
                                <div class="thecrate-custom-variations-inner">

                                    <?php if ($tabs_status) { ?>
                                        <div class="wcpvc-tabs wcpvc-tabs-style-topline">
                                            <nav>
                                                <ul>
                                    <?php } ?>

                                    <?php $attribute_number = 1; ?>
                                    <?php foreach ( $product->get_variation_attributes() as $attribute_name => $options ) : ?>
                                        <?php $sanitized_name = sanitize_title( $attribute_name ); ?>
                                            
                                            <?php if (!$tabs_status) { ?>
                                                <div class="row thecrate-single-variantion-group attribute-<?php echo esc_attr( $sanitized_name ); ?>  <?php echo esc_attr($carousel_item_class); ?>">
                                                    <div class="col-md-12 thecrate-variation-group-title">
                                                        <h3 data-title-class="thecrate-attribute-group-title" class="thecrate-attribute-group-title <?php echo esc_attr( sanitize_title( $attribute_name ) ) . ' ' .esc_attr($attribute_titles_alignment); ?>">
                                                            <?php echo esc_html($attribute_number).'. '.wc_attribute_label( $attribute_name ); // WPCS: XSS ok. ?>:
                                                        </h3>
                                                    </div>

                                                    <?php
                                                        if ( isset( $_REQUEST[ 'attribute_' . $sanitized_name ] ) ) {
                                                            $checked_value = $_REQUEST[ 'attribute_' . $sanitized_name ];
                                                        } elseif ( isset( $selected_attributes[ $sanitized_name ] ) ) {
                                                            $checked_value = $selected_attributes[ $sanitized_name ];
                                                        } else {
                                                            $checked_value = '';
                                                        }
                                                    ?>
                                                    <div class="value col-md-12 thecrate-variation-group-variations <?php echo esc_attr($attribute_boxes_alignment); ?> " id="<?php echo esc_attr($attribute_name); ?>">

                                                        <?php
                                                        if ( ! empty( $options ) ) {
                                                            if ( taxonomy_exists( $attribute_name ) ) {
                                                                // Get terms if this is a taxonomy - ordered. We need the names too.
                                                                $terms = wc_get_product_terms( $product_id, $attribute_name, array( 'fields' => 'all' ) );

                                                                foreach ( $terms as $term ) {
                                                                    if ( ! in_array( $term->slug, $options ) ) {
                                                                        continue;
                                                                    }
                                                                    wcpvc_products_attributes_radio( $product_id, $checked_value, $term->slug, $term->name, $sanitized_name, $active_background_color, $active_border_color );
                                                                }
                                                            } else {
                                                                foreach ( $options as $option ) {
                                                                    wcpvc_products_attributes_radio( $product_id, $checked_value, $option, $option, $sanitized_name, $active_background_color, $active_border_color );
                                                                }
                                                            }
                                                        }
                                                        ?>

                                                    </div>
                                                </div>
                                            <?php }else{ ?>

                                                <?php
                                                    if ( isset( $_REQUEST[ 'attribute_' . $sanitized_name ] ) ) {
                                                        $checked_value = $_REQUEST[ 'attribute_' . $sanitized_name ];
                                                    } elseif ( isset( $selected_attributes[ $sanitized_name ] ) ) {
                                                        $checked_value = $selected_attributes[ $sanitized_name ];
                                                    } else {
                                                        $checked_value = '';
                                                    }
                                                ?>
                                                
                                                <li data-title-class="thecrate-attribute-group-title" class=""><a href="#tab-attribute-<?php echo esc_attr( $sanitized_name ); ?>"><span><?php echo esc_html($attribute_number).'. '.wc_attribute_label( $attribute_name ); // WPCS: XSS ok. ?> </span></a></li>
                                            <?php } ?>

                                        <?php $attribute_number++; ?>
                                    <?php endforeach; ?>
                                    
                                    <?php if ($tabs_status) { ?>
                                            </ul>
                                        </nav>
                                    <?php } ?>

                                    <?php if ($tabs_status) { ?>
                                        <div class="thecrate-single-variantion-group <?php echo esc_attr($tabs_wrapper); ?>">
                                            <?php $attribute_number = 1; ?>
                                            <?php foreach ( $product->get_variation_attributes() as $attribute_name => $options ) : ?>
                                                <?php $sanitized_name = sanitize_title( $attribute_name ); ?>
                                                <div class="thecrate-single-variantion-group attribute-<?php echo esc_attr( $sanitized_name ); ?>  <?php echo esc_attr($carousel_item_class); ?>" id="tab-attribute-<?php echo esc_attr( $sanitized_name ); ?>">
                                                                                               <?php
                                                    if ( isset( $_REQUEST[ 'attribute_' . $sanitized_name ] ) ) {
                                                        $checked_value = $_REQUEST[ 'attribute_' . $sanitized_name ];
                                                    } elseif ( isset( $selected_attributes[ $sanitized_name ] ) ) {
                                                        $checked_value = $selected_attributes[ $sanitized_name ];
                                                    } else {
                                                        $checked_value = '';
                                                    }
                                                    ?>
                                                    <div class="value thecrate-variation-group-variations <?php echo esc_attr($carousel_class); ?> <?php echo esc_attr($attribute_boxes_alignment); ?>" id="<?php echo esc_attr($attribute_name); ?>">

                                                        <?php
                                                        if ( ! empty( $options ) ) {
                                                            if ( taxonomy_exists( $attribute_name ) ) {
                                                                // Get terms if this is a taxonomy - ordered. We need the names too.
                                                                $terms = wc_get_product_terms( $product_id, $attribute_name, array( 'fields' => 'all' ) );

                                                                foreach ( $terms as $term ) {
                                                                    if ( ! in_array( $term->slug, $options ) ) {
                                                                        continue;
                                                                    }
                                                                    wcpvc_products_attributes_radio( $product_id, $checked_value, $term->slug, $term->name, $sanitized_name, $active_background_color, $active_border_color );
                                                                }
                                                            } else {
                                                                foreach ( $options as $option ) {
                                                                    wcpvc_products_attributes_radio( $product_id, $checked_value, $option, $option, $sanitized_name, $active_background_color, $active_border_color );
                                                                }
                                                            }
                                                        }
                                                        ?>

                                                    </div>
                                                </div>

                                                <?php $attribute_number++; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php } ?>

                                    <?php if ($tabs_status) { ?>
                                        </div>
                                    <?php } ?>

                                </div>

                                <?php 
                                    echo end( $attribute_keys ) === $attribute_name ? wp_kses_post( apply_filters( 'woocommerce_reset_variations_link', '<div class="thecrate-reset-options text-center"><a class="reset_variations" href="#">' . esc_html__( '✕ Reset & Start Over', 'wcpvc' ) . '</a></div>' ) ) : '';
                                ?>
                            </div>

                            <?php 
                                $attr_text_default = __('The price will be visible here after completing the configuration.', 'wcpvc');
                                if ($info_before_purchase_btn) {
                                    $attr_text_default = $info_before_purchase_btn;
                                }
                                $attr_text_default_success = __('Please proceed to Add To Cart!', 'wcpvc');
                                if ($info_before_purchase_btn_success) {
                                    $attr_text_default_success = $info_before_purchase_btn_success;
                                }
                            ?>


                            <div class="single_variation_wrap">
                                <p data-attr-text-default="<?php echo esc_attr($attr_text_default); ?>" data-attr-text-success="<?php echo esc_attr($attr_text_default_success); ?>" class="woocommerce-variation-price-tip <?php echo esc_attr($attribute_boxes_alignment); ?>">
                                    <?php echo esc_html($attr_text_default); ?>
                                </p>
                                <div class="woocommerce-variation single_variation" style="display: none;"></div>
                                <div class="woocommerce-variation-add-to-cart variations_button">
                                    <div class="quantity" style="display: block;">
                                        <label class="screen-reader-text" for="quantity_for_<?php echo esc_attr($product_id); ?>"><?php echo esc_html__( 'Quantity', 'wcpvc' ); ?></label>
                                        <input type="number" id="quantity_for_<?php echo esc_attr($product_id); ?>" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="<?php echo esc_attr__( 'Qty', 'wcpvc' ); ?>" size="4" placeholder="" inputmode="numeric">
                                    </div>
                                    <button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html__( 'Confirm & Add to cart', 'wcpvc' ); ?></button>
                                    <input type="hidden" name="add-to-cart" value="<?php echo esc_attr($product_id); ?>">
                                    <input type="hidden" name="product_id" value="<?php echo esc_attr($product_id); ?>">
                                    <input type="hidden" name="variation_id" class="variation_id" value="">
                                </div>
                            </div>
                        <?php endif; ?>
                    </form>
                <?php }else{ ?>
                    <p class="no-margin">
                        <strong><?php echo esc_html__('Product Variations Configurator Error!', 'wcpvc'); ?></strong><br />
                        <?php echo esc_html__('(!) Incorrect Product ID or Product not set.', 'wcpvc'); ?>
                    </p>
                <?php } ?>
        </div>
    <?php } ?>

    <?php   
    $html = ob_get_contents();
    ob_end_clean(); //clean echoed html code

    return $html;

}
add_shortcode('themeslr_pv_configurator', 'wcpvc_product_variations_configurator');


add_action( 'vc_before_init', 'wcpvc_vc_map__pv_configurator' );
function wcpvc_vc_map__pv_configurator() {
    if (function_exists('vc_map')) {
        vc_map(
            array(
                "name" => esc_attr__("ThemeSLR - Product Variations Configurator", 'wcpvc'),
                "base" => "themeslr_pv_configurator",
                "category" => esc_attr__('ThemeSLR', 'wcpvc'),
                "icon" => "themeslr_shortcode",
                "params" => array(
                    array(
                        "group" => "Content",
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_attr__( "Select a Variable Product", 'wcpvc' ),
                        "param_name" => "product_id",
                        "value" => wcpvc_get_variable_products(),
                        "description" => "This configurator can be used only for Variable products (with variations) & Variable Subscription post types *(WooCommerce Subscriptions plugin)"
                    ),
                    array(
                        "group" => "Content",
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_attr__( "Section Title", 'wcpvc' ),
                        "param_name" => "title",
                        "value" => "",
                        "description" => ""
                    ),
                    array(
                        "group" => "Content",
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_attr__( "Section Subtitle", 'wcpvc' ),
                        "param_name" => "subtitle",
                        "value" => "",
                        "description" => ""
                    ),
                    array(
                        "group" => "Content",
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_attr__( "Hide empty title/subtitle replacement texts?", 'wcpvc' ),
                        "param_name" => "hide_empty_fields",
                        "description" => "If the title and the subtitle fields are empty, we have some placeholder texts on the templates. By checking this option, those replacement texts will not be listed.",
                        "value" => "", //Default color
                    ),
                    array(
                        "group" => "Content",
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_attr__( "Info Text Before Add to Cart", 'wcpvc' ),
                        "param_name" => "info_before_purchase_btn",
                        "value" => "",
                        "description" => __('Default: The price will be visible here after completing the configuration.', 'wcpvc'),
                    ),
                    array(
                        "group" => "Content",
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_attr__( "Info Text Before Add to Cart (Success)", 'wcpvc' ),
                        "param_name" => "info_before_purchase_btn_success",
                        "value" => "",
                        "description" => __('Default: Please proceed to Add To Cart!', 'wcpvc'),
                    ),
                    array(
                        "group" => "Alignment",
                        "type" => "dropdown",
                        "heading" => esc_attr__("Title/Subtitle Alignment", 'wcpvc'),
                        "param_name" => "title_subtitle_alignment",
                        "std" => '',
                        "holder" => "div",
                        "class" => "",
                        "description" => "",
                        "value" => array(
                            'Left'   => 'text-left',
                            'Center'   => 'text-center',
                            'Right'   => 'text-right',
                        )
                    ),
                    array(
                        "group" => "Alignment",
                        "type" => "dropdown",
                        "heading" => esc_attr__("Attribute Titles Alignment", 'wcpvc'),
                        "param_name" => "attribute_titles_alignment",
                        "std" => '',
                        "holder" => "div",
                        "class" => "",
                        "description" => "",
                        "value" => array(
                            'Left'   => 'text-left',
                            'Center'   => 'text-center',
                            'Right'   => 'text-right',
                        )
                    ),
                    array(
                        "group" => "Alignment",
                        "type" => "dropdown",
                        "heading" => esc_attr__("Attribute Boxes Content Alignment", 'wcpvc'),
                        "param_name" => "attribute_boxes_alignment",
                        "std" => '',
                        "holder" => "div",
                        "class" => "",
                        "description" => "",
                        "value" => array(
                            'Left'   => 'text-left',
                            'Center'   => 'text-center',
                            'Right'   => 'text-right',
                        )
                    ),
                    array(
                        "group" => "Styling",
                        "type" => "colorpicker",
                        "class" => "",
                        "heading" => esc_attr__( "Active Box Background color", 'wcpvc' ),
                        "param_name" => "active_background_color",
                        "value" => "", //Default color
                    ),
                    array(
                        "group" => "Styling",
                        "type" => "colorpicker",
                        "class" => "",
                        "heading" => esc_attr__( "Active Box Border color", 'wcpvc' ),
                        "param_name" => "active_border_color",
                        "value" => "", //Default color
                    ),
                    array(
                        "group" => "Styling",
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_attr__( "Attributes in Tabs?", 'wcpvc' ),
                        "param_name" => "tabs_status",
                        "value" => "", //Default color
                    ),
                    array(
                        "group" => "Styling",
                        "type" => "checkbox",
                        "class" => "",
                        "heading" => esc_attr__( "Enable Carousel (Only on mobiles on resolutions < 767px)", 'wcpvc' ),
                        "param_name" => "carousel_status",
                        "value" => "", //Default color
                    ),
                )
            )
        );
    }
}