<?php 
// SHORTCODES
include_once( 'tslr-product-configurator/tslr-product-configurator.php' );

// Elementor
if ( class_exists('Elementor\Core\Admin\Admin') ) {

	class WCPVC_ThemeSLR_Elementor_Widgets {

		protected static $instance = null;

		public static function get_instance() {
			if ( ! isset( static::$instance ) ) {
				static::$instance = new static;
			}

			return static::$instance;
		}

		protected function __construct() {
			require_once( 'tslr-product-configurator/tslr-product-configurator.elementor.php' );
			add_action( 'elementor/widgets/widgets_registered', [ $this, 'register_widgets' ] );
		}

		public function register_widgets() {
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\wcpvc_product_variations_configurator_widget() );
		}

	}

	add_action( 'init', 'wcpvc_elementor_init' );
	function wcpvc_elementor_init() {
		WCPVC_ThemeSLR_Elementor_Widgets::get_instance();
	}

	function wcpvc_elementor_widget_categories( $elements_manager ) {

		$elements_manager->add_category(
			'themeslr-widgets',
			[
				'title' => __( 'ThemeSLR', 'wcpvc' ),
				'icon' => 'eicon-gallery-grid',
			]
		);

	}
	add_action( 'elementor/elements/categories_registered', 'wcpvc_elementor_widget_categories' );

}