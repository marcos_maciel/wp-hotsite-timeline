<?php

if ( ! class_exists( 'WCPVC_Taxonomy_Field' ) ) {

      class WCPVC_Taxonomy_Field {

            public function __construct() {
                  //
            }
         
            /*
            * Initialize the class and start calling our hooks and filters
            * @since 1.0.0
            */
            public function init() {
                  if (is_admin()) {
                        $attribute_taxonomies = wc_get_attribute_taxonomies();
                        if ( $attribute_taxonomies )
                        {
                            foreach ($attribute_taxonomies as $tax)
                            {
                                // if (taxonomy_exists(wc_attribute_taxonomy_name($tax->attribute_name)))
                                // {
                                    /** The taxonomy we want to parse */
                                    $taxonomy_name = wc_attribute_taxonomy_name($tax->attribute_name);
                                    add_action( $taxonomy_name.'_add_form_fields', array ( $this, 'add_category_image' ), 10, 2 );
                                    add_action( 'created_'.$taxonomy_name, array ( $this, 'save_category_image' ), 10, 2 );
                                    add_action( $taxonomy_name.'_edit_form_fields', array ( $this, 'update_category_image' ), 10, 2 );
                                    add_action( 'edited_'.$taxonomy_name, array ( $this, 'updated_category_image' ), 10, 2 );
                                    add_action( 'admin_enqueue_scripts', array( $this, 'load_media' ) );
                                    add_action( 'admin_footer', array ( $this, 'add_script' ) );
                                // }
                            }
                        }
                  }
            }

            public function load_media() {
                  wp_enqueue_media();
            }
         
            /*
             * Add a form field in the new category page
             * @since 1.0.0
            */
            public function add_category_image ( $taxonomy ) { ?>
                  <div class="form-field term-group">
                  <label for="category-image-id"><?php esc_html_e('Image', 'wcpvc'); ?></label>
                  <input type="hidden" id="category-image-id" name="category-image-id" class="custom_media_url" value="">
                  <div id="category-image-wrapper"></div>
                        <p>
                              <input type="button" class="button button-secondary ct_tax_media_button" id="ct_tax_media_button" name="ct_tax_media_button" value="<?php esc_html_e( 'Add Image', 'wcpvc' ); ?>" />
                              <input type="button" class="button button-secondary ct_tax_media_remove" id="ct_tax_media_remove" name="ct_tax_media_remove" value="<?php esc_html_e( 'Remove Image', 'wcpvc' ); ?>" />
                        </p>
                  </div>
                  <?php
            }
         
            /*
             * Save the form field
             * @since 1.0.0
            */
            public function save_category_image ( $term_id, $tt_id ) {
                  if( isset( $_POST['category-image-id'] ) && '' !== $_POST['category-image-id'] ){
                        $image = $_POST['category-image-id'];
                        add_term_meta( $term_id, 'category-image-id', $image, true );
                  }
            }
         
            /*
             * Edit the form field
             * @since 1.0.0
            */
            public function update_category_image ( $term, $taxonomy ) { ?>
                  <tr class="form-field term-group-wrap">
                        <th scope="row">
                              <label for="category-image-id"><?php esc_html_e( 'Image', 'wcpvc' ); ?></label>
                        </th>
                        <td>
                              <?php $image_id = get_term_meta ( $term -> term_id, 'category-image-id', true ); ?>
                              <input type="hidden" id="category-image-id" name="category-image-id" value="<?php echo esc_attr($image_id); ?>">
                              <div id="category-image-wrapper">
                                    <?php if ( $image_id ) { ?>
                                          <?php echo wp_get_attachment_image ( $image_id, 'thumbnail' ); ?>
                                    <?php } ?>
                              </div>
                              <p>
                                    <input type="button" class="button button-secondary ct_tax_media_button" id="ct_tax_media_button" name="ct_tax_media_button" value="<?php esc_html_e( 'Add Image', 'wcpvc' ); ?>" />
                                    <input type="button" class="button button-secondary ct_tax_media_remove" id="ct_tax_media_remove" name="ct_tax_media_remove" value="<?php esc_html_e( 'Remove Image', 'wcpvc' ); ?>" />
                              </p>
                        </td>
                  </tr>
                  <?php
            }

            /*
            * Update the form field value
            * @since 1.0.0
            */
            public function updated_category_image ( $term_id, $tt_id ) {
                  if( isset( $_POST['category-image-id'] ) && '' !== $_POST['category-image-id'] ){
                        $image = $_POST['category-image-id'];
                        update_term_meta ( $term_id, 'category-image-id', $image );
                  } else {
                        update_term_meta ( $term_id, 'category-image-id', '' );
                  }
            }

            /*
            * Add script
            * @since 1.0.0
            */
            public function add_script() { ?>
                  <script>
                        (function ($) {
                              'use strict';

                              jQuery(document).ready( function($) {
                                    function themeslr_vc_media_upload(button_class) {
                                          var _custom_media = true,
                                          _orig_send_attachment = wp.media.editor.send.attachment;
                                          $('body').on('click', button_class, function(e) {
                                                var button_id = '#'+$(this).attr('id');
                                                var send_attachment_bkp = wp.media.editor.send.attachment;
                                                var button = $(button_id);
                                                _custom_media = true;
                                                wp.media.editor.send.attachment = function(props, attachment){
                                                      if ( _custom_media ) {
                                                            $('#category-image-id').val(attachment.id);
                                                            $('#category-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
                                                            $('#category-image-wrapper .custom_media_image').attr('src',attachment.url).css('display','block');
                                                      } else {
                                                            return _orig_send_attachment.apply( button_id, [props, attachment] );
                                                      }
                                                }
                                                wp.media.editor.open(button);
                                                return false;
                                          });
                                    }

                                    themeslr_vc_media_upload('.ct_tax_media_button.button'); 
                                    $('body').on('click','.ct_tax_media_remove',function(){
                                          $('#category-image-id').val('');
                                          $('#category-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
                                    });

                                    // Thanks: http://stackoverflow.com/questions/15281995/wordpress-create-category-ajax-response
                                    $(document).ajaxComplete(function(event, xhr, settings) {
                                          var queryStringArr = settings.data.split('&');
                                          if( $.inArray('action=add-tag', queryStringArr) !== -1 ){
                                                var xml = xhr.responseXML;
                                                $response = $(xml).find('term_id').text();
                                                if($response!=""){
                                                      // Clear the thumb image
                                                      $('#category-image-wrapper').html('');
                                                }
                                          }
                                    });
                              });
                        
                        })(jQuery);
                  </script>
            <?php }
      }
     
      function wcpvc_init_class() {
          $WCPVC_Taxonomy_Field = new WCPVC_Taxonomy_Field();
          $WCPVC_Taxonomy_Field->init();
      }
      add_action( 'plugins_loaded', 'wcpvc_init_class');
}



function wcpvc_edit_attribute_icon() {
    $id = isset( $_GET['edit'] ) ? absint( $_GET['edit'] ) : 0;
    $value = $id ? get_option( "attribute_icon-$id" ) : '';
    ?>
        <tr class="form-field">
            <th scope="row" valign="top">
                <label for="wcpvc_attribute_icon"><?php echo esc_html__('Select Icon', 'wcpvc'); ?></label>
            </th>
            <td>
                <input type="text" name="wcpvc_attribute_icon" id="wcpvc_attribute_icon" value="<?php echo esc_attr( $value ); ?>" />
            </td>
        </tr>
    <?php
}
// add_action( 'woocommerce_after_add_attribute_fields', 'wcpvc_edit_attribute_icon' );
// add_action( 'woocommerce_after_edit_attribute_fields', 'wcpvc_edit_attribute_icon' );


function wcpvc_save_attribute_icon( $id ) {
    if ( is_admin() && isset( $_POST['wcpvc_attribute_icon'] ) ) {
        $option = "attribute_icon-$id";
        update_option( $option, sanitize_text_field( $_POST['wcpvc_attribute_icon'] ) );
    }
}
// add_action( 'woocommerce_attribute_added', 'wcpvc_save_attribute_icon' );
// add_action( 'woocommerce_attribute_updated', 'wcpvc_save_attribute_icon' );


// add_action( 'woocommerce_attribute_deleted', function ( $id ) {
//     delete_option( "attribute_icon-$id" );
// } );

// $term = get_queried_object();
// $attr_id = wc_attribute_taxonomy_id_by_name( $term->taxonomy );
// $wcpvc_attribute_icon = get_option( "attribute_icon-$attr_id" );