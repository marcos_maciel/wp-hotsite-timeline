/*------------------------------------------------------------------
[Custom JS Scripts]

[Table of contents]

- PVC Functions

-------------------------------------------------------------------*/

(function ($) {
    'use strict';

    // Product Configurator
    function wcpvc_set_selected_box(){
        jQuery('.thecrate-variation-group-variations input:radio').change(function(){
            
            // reset all selected items
            jQuery(this).parent().parent().parent().find('.thecrate-single-variation-box').removeClass('selected');
            
            // set selected checkmark
            var $self = jQuery(this);
            if ($self.prop('checked')) {
                $self.parent().addClass('selected');
            }

            // update variation tip
            var variation_tip = jQuery('.woocommerce-variation-price-tip'),
                variation_tip_wrap = jQuery('.single_variation_wrap'),
                variation_tip_text_default = jQuery('.woocommerce-variation-price-tip').attr('data-attr-text-default'),
                variation_tip_text_success = jQuery('.woocommerce-variation-price-tip').attr('data-attr-text-success'),
                count_selected_items = jQuery('.thecrate-single-variation-box.selected').length,
                count_titles = jQuery('.thecrate-custom-variations-inner *[data-title-class="thecrate-attribute-group-title"]').length; 

            if (count_selected_items >= count_titles) {
                variation_tip.text(variation_tip_text_success);
                variation_tip_wrap.addClass('wcpvc-success')
            }else{
                variation_tip.text(variation_tip_text_default);
                variation_tip_wrap.removeClass('wcpvc-success')
            }

        });
    }
    $(document).ready(function(e) {
        wcpvc_set_selected_box();
    });


    // Nested Carousel
    function wcpvc_owl_initialize() {
       if ($(window).width() < 1100) {
            WCPVC_Carousel_Nested.init();
       }else{
            WCPVC_Carousel_Nested_Destroy.init();
       }
    }

    $(document).ready(function(e) {
        wcpvc_owl_initialize();
    });

    $(window).resize(function() {
        wcpvc_owl_initialize();
    });

    // OWL Start
    var WCPVC_Carousel_Nested = {
        init: function () {
            var vcPvcConfigurators_Nested = $('.thecrate-variation-group-variations.thecrate-custom-variations-inner-carousel');
            
            if (vcPvcConfigurators_Nested.length) {
                
                vcPvcConfigurators_Nested.each(function (i) {
           
                    $(vcPvcConfigurators_Nested).owlCarousel({
                        loop:false,
                        mouseDrag:true,
                        touchDrag:true,
                        margin:15,
                        responsiveClass:true,
                        dots:true,
                        responsive:{
                            0:{
                                items:1,
                                nav:true
                            },
                            600:{
                                items:2,
                                nav:true
                            },
                            1000:{
                                items:3,
                                nav:true,
                            }
                        }
                    });
                    
                });
            }
        }
    };

    // OWL Destroy
    var WCPVC_Carousel_Nested_Destroy = {
        init: function () {
            var vcPvcConfigurators_Nested_Destroy = $('.thecrate-variation-group-variations.thecrate-custom-variations-inner-carousel');
            if (vcPvcConfigurators_Nested_Destroy.length) {
                vcPvcConfigurators_Nested_Destroy.each(function (i) {
                    $(vcPvcConfigurators_Nested_Destroy).owlCarousel('destroy');
                    
                });
            }
        }
    };


    // Attributes tabs
    [].slice.call( document.querySelectorAll( '.wcpvc-tabs' ) ).forEach( function( el ) {
        new CBPFWTabs( el );
    });

} (jQuery) );